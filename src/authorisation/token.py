from datetime import datetime, timedelta
from typing import Protocol
from uuid import UUID, uuid4

from jose import ExpiredSignatureError, JWTError, jwt
from jose.exceptions import JWTClaimsError

from src.config import settings


class JWTSecretManagerProtocol(Protocol):
    @property
    def encoding_secret(self) -> str:
        ...

    @property
    def decoding_secret(self) -> str:
        ...

    @property
    def algorithm(self) -> str:
        ...


class JWTSecretManager:
    def __read_file(self, file_path) -> str:
        with open(file_path, "r") as file:
            contents = file.read()
        return contents

    @property
    def encoding_secret(self) -> str:
        if settings.JWT_ALGORITHM == "HS256":
            assert settings.JWT_SECRET is not None
            return settings.JWT_SECRET
        else:
            file = self.__read_file(settings.JWT_PRIVATE_KEY_FILE)
            return file

    @property
    def decoding_secret(self):
        if settings.JWT_ALGORITHM == "HS256":
            assert settings.JWT_SECRET is not None
            return settings.JWT_SECRET
        else:
            file = self.__read_file(settings.JWT_PUBLIC_KEY_FILE)
            return file

    @property
    def algorithm(self):
        return settings.JWT_ALGORITHM


class JWTToken:
    token_type: str | None = None
    _jwt_secret_manager: JWTSecretManagerProtocol = JWTSecretManager()
    expire_time_seconds: int = 60 * 5

    __decode_option: dict = {
        "require_iat": True,
        "require_exp": True,
        "require_iss": True,
        "require_jti": True,
        "leeway": 60,
    }

    def __init__(self, token_string: str):
        assert isinstance(token_string, str)
        self.__raw_token = token_string
        self.__header: dict[str, str] = jwt.get_unverified_header(token_string)
        self.__payload: dict[str, str] = jwt.get_unverified_claims(token_string)

    @property
    def header(self):
        return self.__header

    @property
    def payload(self):
        return self.__payload

    def is_valid(self, raise_error=False) -> bool:
        secret = self._jwt_secret_manager.decoding_secret
        algorithm = self._jwt_secret_manager.algorithm
        try:
            jwt.decode(
                token=self.__raw_token,
                key=secret,
                options=self.__decode_option,
                algorithms=algorithm,
            )
        except (JWTError, ExpiredSignatureError, JWTClaimsError) as e:
            if raise_error:
                raise e
            return False
        return True

    def is_expired(self) -> bool:
        try:
            self.is_valid(raise_error=True)
        except ExpiredSignatureError:
            return False
        finally:
            return True

    def _default_payload(self) -> dict:
        default_payload = {
            "exp": datetime.utcnow() + timedelta(seconds=self.expire_time_seconds),
            "iat": datetime.utcnow(),
            "iss": "web-libros",
            "jti": uuid4().hex,
        }
        if self.token_type:
            default_payload["type"] = self.token_type
        return default_payload

    @classmethod
    def encode(cls, to_encode: dict[str, str]):
        claim = {**cls._default_payload(cls), **to_encode}
        secret = cls._jwt_secret_manager.encoding_secret
        algorithm = cls._jwt_secret_manager.algorithm
        jwt_str = jwt.encode(claim, secret, algorithm=algorithm)
        return cls(jwt_str)

    def __str__(self):
        return self.__raw_token

    def __repr__(self):
        return f"JWTToken({self.__raw_token})"


class UserProtocol(Protocol):
    @property
    def uuid(self) -> UUID:
        ...


class JWTUserAccesToken(JWTToken):
    token_type = "access"
    expire_time_seconds = settings.JWT_ACCESS_TOKEN_EXPIRE_SECONDS

    @classmethod
    def encode(cls, user: UserProtocol):
        to_encode = {"user": str(user.uuid)}
        return super(JWTUserAccesToken, cls).encode(to_encode)

    def is_access_token(self):
        token_type = self.payload.get("type")
        user = self.payload.get("user")
        return token_type == "access" and user is not None


class JWTUserRefreshToken(JWTToken):
    token_type = "refresh"
    expire_time_seconds = settings.JWT_REFRESH_TOKEN_EXPIRE_SECONDS

    @classmethod
    def encode(cls, user: UserProtocol):
        to_encode = {"user": str(user.uuid)}
        return super(JWTUserRefreshToken, cls).encode(to_encode)

    def is_refresh_token(self):
        token_type = self.payload.get("type")
        user = self.payload.get("user")
        return token_type == "refresh" and user is not None
