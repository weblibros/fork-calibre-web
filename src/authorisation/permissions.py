from uuid import UUID

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import ExpiredSignatureError, JWTError
from jose.exceptions import JWTClaimsError

from src.authentication import urls
from src.authorisation.token import JWTUserAccesToken

oauth2_scheme = OAuth2PasswordBearer(tokenUrl=urls.PASSWORD_LOGIN_USER_URL)


def valid_jwt_access_token(token: str = Depends(oauth2_scheme)) -> JWTUserAccesToken:
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    expired_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="credentials are expired",
        headers={"WWW-Authenticate": "Bearer"},
    )
    jwt_token = JWTUserAccesToken(token_string=token)
    try:
        jwt_token.is_valid(raise_error=True)
    except ExpiredSignatureError:
        raise expired_exception
    except (JWTError, JWTClaimsError):
        raise credentials_exception

    if jwt_token.is_access_token() is not True:
        raise credentials_exception

    return jwt_token


def request_user_uuid(
    token: JWTUserAccesToken = Depends(valid_jwt_access_token),
) -> UUID:
    return UUID(token.payload["user"])
