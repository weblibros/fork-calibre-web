from pydantic import BaseModel

from src.authorisation.fields import JWTTokenType


class JwtKeyPair(BaseModel):
    token_type: str = "bearer"
    access_token: JWTTokenType
    refresh_token: JWTTokenType
