from src.authorisation.token import JWTToken


class JWTTokenType(JWTToken):
    @classmethod
    def __modify_schema__(cls, field_schema):
        # __modify_schema__ should mutate the dict it receives in place,
        # the returned value will be ignored
        field_schema.update(
            type="string",
        )

    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v) -> str:
        if not isinstance(v, JWTToken):
            raise TypeError("Invalid value")
        v.is_valid(raise_error=True)
        return str(v)
