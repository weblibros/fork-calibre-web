from src.authorisation.schema import JwtKeyPair
from src.authorisation.token import JWTToken


def test_jwtkeypair_dict_output():
    jwt_access_token = JWTToken.encode({"token_type": "access"})
    jwt_refresh_token = JWTToken.encode({"token_type": "refresh"})

    schema = JwtKeyPair(
        access_token=jwt_access_token,
        refresh_token=jwt_refresh_token,
    )

    dict_schema = schema.dict()

    assert "access_token" in dict_schema.keys()
    assert "refresh_token" in dict_schema.keys()
    access_token = dict_schema.get("access_token")
    refresh_token = dict_schema.get("refresh_token")
    assert isinstance(access_token, str)
    assert isinstance(jwt_access_token, JWTToken)
    assert access_token == str(jwt_access_token)
    assert access_token != jwt_access_token
    assert isinstance(refresh_token, str)
    assert isinstance(jwt_refresh_token, JWTToken)
    assert refresh_token == str(jwt_refresh_token)
    assert refresh_token != jwt_refresh_token


def test_jwtkeypair_schema_output():
    jwt_access_token = JWTToken.encode({"token_type": "access"})
    jwt_refresh_token = JWTToken.encode({"token_type": "refresh"})

    schema = JwtKeyPair(
        access_token=jwt_access_token,
        refresh_token=jwt_refresh_token,
    )

    schema_output = schema.schema()

    assert schema_output == {
        "properties": {
            "access_token": {
                "title": "Access Token",
                "type": "string",
            },
            "token_type": {
                "default": "bearer",
                "title": "Token Type",
                "type": "string",
            },
            "refresh_token": {
                "title": "Refresh Token",
                "type": "string",
            },
        },
        "required": [
            "access_token",
            "refresh_token",
        ],
        "title": "JwtKeyPair",
        "type": "object",
    }
