from src.authorisation.token_factory import user_jwt_tokens_factory


def test_user_jwt_token_factory(user):

    user_tokens = user_jwt_tokens_factory(user)

    assert "access_token" in user_tokens.keys()
    assert "refresh_token" in user_tokens.keys()
    acces_token = user_tokens.get("access_token")
    refresh_token = user_tokens.get("refresh_token")
    assert acces_token.payload.get("user") == str(user.uuid)
    assert acces_token.payload.get("type") == "access"
    assert refresh_token.payload.get("user") == str(user.uuid)
    assert refresh_token.payload.get("type") == "refresh"
