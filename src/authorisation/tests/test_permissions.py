from datetime import datetime, timedelta
from uuid import uuid4

import pytest
from fastapi import HTTPException
from jose import jwt

from src.authorisation.permissions import request_user_uuid, valid_jwt_access_token
from src.authorisation.token import JWTToken, JWTUserAccesToken


def test_valid_jwt_access_token(access_token_user):
    raw_token = str(access_token_user)
    valid_token = valid_jwt_access_token(raw_token)

    assert isinstance(valid_token, JWTUserAccesToken)
    assert valid_token is not access_token_user
    assert valid_token.payload == access_token_user.payload


def test_unvalid_jwt_access_toke(user):
    test_payload = {
        "user": str(user.uuid),
        "type": "access",
        "exp": datetime.utcnow() + timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_token = jwt.encode(
        test_payload,
        "notcorrect secret",
        algorithm="HS256",
    )
    with pytest.raises(HTTPException) as e:
        valid_jwt_access_token(raw_token)

    assert e.value.detail == "Could not validate credentials"
    assert e.value.status_code == 401


def test_expired_jwt_access_toke(user):
    test_payload = {
        "user": str(user.uuid),
        "type": "access",
        "exp": datetime.utcnow() - timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_token = str(JWTToken.encode(test_payload))
    with pytest.raises(HTTPException) as e:
        valid_jwt_access_token(raw_token)

    assert e.value.detail == "credentials are expired"
    assert e.value.status_code == 401


def test_jwt_no_access_toke(user):
    test_payload = {
        "user": str(user.uuid),
        "type": "no-access",
        "exp": datetime.utcnow() + timedelta(seconds=3600),
        "iat": datetime.utcnow(),
        "iss": "web-libros",
        "jti": str(uuid4()),
    }
    raw_token = str(JWTToken.encode(test_payload))
    with pytest.raises(HTTPException) as e:
        valid_jwt_access_token(raw_token)

    assert e.value.detail == "Could not validate credentials"
    assert e.value.status_code == 401


def test_request_user_uuid(user):
    jwt_token = JWTUserAccesToken.encode(user)
    user_uuid = request_user_uuid(jwt_token)
    assert user_uuid == user.uuid
