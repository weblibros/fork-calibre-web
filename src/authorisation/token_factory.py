from src.authorisation.token import JWTToken, JWTUserAccesToken, JWTUserRefreshToken
from src.users.models import User


def user_jwt_tokens_factory(user: User) -> dict[str, JWTToken]:
    return {
        "access_token": JWTUserAccesToken.encode(user),
        "refresh_token": JWTUserRefreshToken.encode(user),
    }
