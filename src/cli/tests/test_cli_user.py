from pydantic import EmailStr

from src.cli.user import async_create_user_from_cli, async_remove_user_from_cli
from src.users.models import User


async def test_cli_create_user():
    email = EmailStr("test_cli_create_user@fake.com")
    user = await async_create_user_from_cli(email, password="test")
    assert isinstance(user, User)
    assert user.email == email
    await async_remove_user_from_cli(email)


async def test_cli_create_user_no_exp_called_twice():
    email = EmailStr("test_cli_create_user_called_twice@fake.com")
    user = await async_create_user_from_cli(email, password="test")
    await async_create_user_from_cli(email, password="test")
    assert isinstance(user, User)
    await async_remove_user_from_cli(email)
