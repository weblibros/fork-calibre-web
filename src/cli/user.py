from asyncio import run as aiorun

import typer
from pydantic import EmailStr

from src.config.database import async_session
from src.users.models import User
from src.users.queries import create, remove, selector

typer_user = typer.Typer()


async def async_create_user_from_cli(email: EmailStr, password: str) -> User | None:
    print(f"creating user: {email}")
    async with async_session() as session:
        user_exist = await selector.find_user_by_email(session, email)
        if user_exist:
            print(f"email: {email} is already in use")
            user = None
        else:
            user = await create.create_user(session, password=password, email=email)
            print(f"user {user.email} created")
    return user


async def async_remove_user_from_cli(email: str):
    print(f"remove user: {email}")
    async with async_session() as session:
        user = await selector.find_user_by_email(session, email)
        if user:
            await remove.remove_user(session, user)
            print(f"user {email} was removed database")
        else:
            print(f"user {email} was not found in database")


@typer_user.command("create")
def cli_create_user(email: str, password: str):
    aiorun(async_create_user_from_cli(password=password, email=EmailStr(email)))


@typer_user.command("remove")
def cli_remove_user(email: str):
    aiorun(async_remove_user_from_cli(email=email))
