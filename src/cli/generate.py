import typer

from src.cli.user import cli_create_user

typer_generate = typer.Typer()


@typer_generate.command("fake-data")
def generate_fake_data():
    fake_users = [
        {"email": "user@fake.com", "password": "Q12345678!"},
    ]
    for user_info in fake_users:
        cli_create_user(**user_info)
