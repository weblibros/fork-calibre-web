import typer

from src.cli import generate, user

typer_app = typer.Typer()


typer_app.add_typer(user.typer_user, name="user")
typer_app.add_typer(generate.typer_generate, name="generate")
