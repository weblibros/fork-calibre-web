from uuid import UUID

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from src.authorisation.permissions import request_user_uuid
from src.config.database import get_async_session
from src.users import urls
from src.users.models import User
from src.users.queries import selector

user_router = APIRouter()


@user_router.get(urls.USER_ME_URL, response_model=User)
async def user_me(
    session: AsyncSession = Depends(get_async_session),
    user_uuid: UUID = Depends(request_user_uuid),
):
    user = await selector.find_user_by_uuid(session, user_uuid)
    return user
