from uuid import UUID, uuid4

from pydantic import EmailStr
from sqlmodel import Column, Field, SQLModel, String


class User(SQLModel, table=True):
    uuid: UUID = Field(
        default_factory=uuid4,
        primary_key=True,
        nullable=False,
    )
    email: EmailStr = Field(sa_column=Column("email", String, unique=True))
    password: str = Field(..., exclude=True)
