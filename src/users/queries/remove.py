from sqlalchemy.ext.asyncio import AsyncSession

from src.users.models import User
from src.users.queries.selector import find_user_by_uuid


async def remove_user(session: AsyncSession, user: User):
    found_user = await find_user_by_uuid(session, user.uuid)
    if found_user is None:
        raise Exception(f"user {user.email} you try to delete does not exits")

    await session.delete(user)
    await session.commit()
