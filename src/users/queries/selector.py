from uuid import UUID

from sqlalchemy.ext.asyncio import AsyncSession
from sqlmodel import select

from src.users.models import User


async def find_user_by_kwargs(session: AsyncSession, **kwargs) -> User | None:

    query = select(User).filter_by(**kwargs)
    result = await session.execute(query)
    user = result.scalar_one_or_none()
    return user


async def find_user_by_email(session: AsyncSession, email: str) -> User | None:

    return await find_user_by_kwargs(session, email=email)


async def find_user_by_uuid(session: AsyncSession, uuid: UUID) -> User | None:

    return await find_user_by_kwargs(session, uuid=uuid)
