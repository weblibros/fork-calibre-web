from pydantic import EmailStr
from sqlalchemy.ext.asyncio import AsyncSession

from src.authentication import password_hasher
from src.users.models import User


async def create_user(session: AsyncSession, email: str, password: str) -> User:

    hashed_password = password_hasher.hash_password(password)

    user = User(email=EmailStr(email), password=hashed_password)
    try:
        session.add(user)
        await session.commit()
    except Exception as e:
        await session.rollback()
        raise Exception(e)

    return user
