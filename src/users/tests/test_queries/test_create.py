import pytest

from src.authentication import password_hasher
from src.users.models import User
from src.users.queries.create import create_user
from src.users.queries.selector import find_user_by_email, find_user_by_uuid


async def test_create_user(session):
    email = "test_create_user@fake.com"
    password = "secretpass"
    user = await create_user(
        session,
        email=email,
        password=password,
    )
    assert isinstance(user, User)
    user = await find_user_by_uuid(session, user.uuid)
    assert user is not None
    assert user.email == email
    assert user.password != password
    valid_password = password_hasher.verify_password(
        raw_password=password,
        hashed_password=user.password,
    )
    assert valid_password

    await session.delete(user)
    await session.commit()


async def test_create_user_exits(session, user):

    email = "user@fake.com"
    password = "secretpass"
    with pytest.raises(Exception) as e:
        await create_user(
            session,
            email=email,
            password=password,
        )
    user_found = await find_user_by_email(session, email)
    assert user_found is not None
    assert (
        "duplicate key value violates unique constraint"
        or "UNIQUE constraint failed" in str(e.value)
    )
