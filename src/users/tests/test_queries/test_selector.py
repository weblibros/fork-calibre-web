from uuid import uuid4

import pytest
from pydantic import EmailStr

from src.users.models import User
from src.users.queries.selector import find_user_by_email, find_user_by_uuid


@pytest.fixture(scope="class")
async def one_user(session):

    user = User(
        email=EmailStr("test@fake.com"),
        password="superpassword",
    )
    session.add(user)
    await session.commit()
    yield user
    await session.delete(user)
    await session.commit()


@pytest.mark.usefixtures("one_user")
class TestUserSelector:
    async def test_get_user_by_email(self, session, one_user):
        user = await find_user_by_email(session, one_user.email)
        assert isinstance(user, User)
        assert user.uuid == one_user.uuid

    async def test_get_no_user_by_email(self, session):
        user = await find_user_by_email(session, "doesnotexits@fake.com")
        assert user is None

    async def test_get_user_by_uuid(self, session, one_user):
        user = await find_user_by_uuid(session, one_user.uuid)
        assert isinstance(user, User)
        assert user.uuid == one_user.uuid

    async def test_get_no_user_by_uuid(self, session):
        user = await find_user_by_uuid(session, uuid4())
        assert user is None
