import pytest
from pydantic import EmailStr

from src.users.models import User
from src.users.queries.create import create_user
from src.users.queries.remove import remove_user
from src.users.queries.selector import find_user_by_uuid


async def test_remove_user(session):
    email = "test_remove_user@fake.com"
    password = "secretpass"
    user = await create_user(
        session,
        email=email,
        password=password,
    )
    user_uuid = user.uuid
    await remove_user(session, user)
    user = await find_user_by_uuid(session, user_uuid)
    assert user is None


async def test_remove_user_not_saved(session):

    email = "test_remove_user@fake.com"
    password = "secretpass"
    user = User(email=EmailStr(email), password=password)
    user_uuid = user.uuid
    found_user = await find_user_by_uuid(session, user_uuid)
    assert found_user is None
    with pytest.raises(Exception) as e:
        await remove_user(session, user)
    assert (
        "duplicate key value violates unique constraint"
        or "UNIQUE constraint failed" in str(e.value)
    )
