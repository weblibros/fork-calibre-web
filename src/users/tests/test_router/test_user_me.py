from src.users import urls


class TestUserMe:
    async def test_user_me(self, client, auth_header_user, user):
        response = await client.get(
            urls.USER_ME_URL,
            headers=auth_header_user,
        )
        assert response.status_code == 200
        result = response.json()
        assert "password" not in result.keys()
        assert "uuid" in result.keys()
        assert "email" in result.keys()
        assert result.get("uuid") == str(user.uuid)
        assert result.get("email") == user.email
