from uuid import UUID

from pydantic import EmailStr

from src.users.models import User


def test_create_model():
    email = EmailStr("test@fake.com")
    password = "fakepass"
    user = User(email=email, password=password)
    assert isinstance(user, User)
    assert isinstance(user.uuid, UUID)
    assert isinstance(user.email, str)
    assert isinstance(user.password, str)
