import enum
from typing import Optional

from pydantic import BaseSettings, PostgresDsn


class LogLevel(str, enum.Enum):  # noqa: WPS600
    """Possible log levels."""

    NOTSET = "NOTSET"
    DEBUG = "DEBUG"
    INFO = "INFO"
    WARNING = "WARNING"
    ERROR = "ERROR"
    FATAL = "FATAL"


class Settings(BaseSettings):
    """
    Application settings.

    These parameters can be configured
    with environment variables.
    """

    PROJECT_NAME = "Firmware-update-gateway"

    UVICORN_PORT = 8000
    UVICORN_HOST = "0.0.0.0"

    # Current environment
    ENVIRONMENT: str = "production"

    LOG_LEVEL: LogLevel = LogLevel.INFO
    LOG_DB: bool = False

    SENTRY_DNS: Optional[str] = None

    DB_DSN: str | PostgresDsn

    JWT_ALGORITHM: str
    JWT_ACCESS_TOKEN_EXPIRE_SECONDS: int = 60 * 5
    JWT_REFRESH_TOKEN_EXPIRE_SECONDS: int = 60 * 60 * 24
    JWT_SECRET: Optional[str] = None
    JWT_PRIVATE_KEY_FILE: Optional[str] = None
    JWT_PUBLIC_KEY_FILE: Optional[str] = None

    CORS_ORIGIN_LIST: list[str] = ["*"]

    class Config:
        # for kubernetes secrets
        secrets_dir = "/var/run"

        use_enum_values = True
        validate_all = True


settings = Settings()
