import sys
from typing import AsyncIterator

from alembic import command, config
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlmodel import SQLModel

from src.config.settings import settings


def create_engine(url: str):
    use_echo = settings.LOG_DB
    return create_async_engine(
        url,
        echo=use_echo,
        future=True,  # use the sqlalchemy 2.0 classes
    )


def generate_test_db_dsn(dsn: str) -> str:
    part_dsn, db_name = dsn.rsplit("/", 1)
    return f"{part_dsn}/test_{db_name}"


engine = create_engine(settings.DB_DSN)
if "pytest" in sys.modules:
    # a bug forces us recreate the engine and pint it to the testing database
    engine = create_engine(generate_test_db_dsn(settings.DB_DSN))


async def init_db():
    async with engine.begin() as conn:
        # await conn.run_sync(SQLModel.metadata.drop_all)
        await conn.run_sync(SQLModel.metadata.create_all)


def run_upgrade(connection, cfg):
    cfg.attributes["connection"] = connection
    command.upgrade(cfg, "head")


async def init_test_db():
    async with engine.begin() as conn:
        # await conn.run_sync(SQLModel.metadata.drop_all)
        # await conn.run_sync(SQLModel.metadata.create_all)
        await conn.run_sync(run_upgrade, config.Config("alembic.ini"))


async_session = sessionmaker(engine, class_=AsyncSession, expire_on_commit=False)


async def get_async_session() -> AsyncIterator[AsyncSession]:
    async with async_session() as session:
        yield session
