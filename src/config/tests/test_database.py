import pytest

from src.config.database import generate_test_db_dsn


@pytest.mark.parametrize(
    "value,expected",
    [
        (
            "postgresql+asyncpg://web_libro_user:devPassword@postgres:5432/web_libro_db",
            "postgresql+asyncpg://web_libro_user:devPassword@postgres:5432/test_web_libro_db",
        ),
        (
            "sqlite+aiosqlite:///database.db",
            "sqlite+aiosqlite:///test_database.db",
        ),
    ],
)
def test_generate_test_db_dsn(value, expected):
    test_dsn = generate_test_db_dsn(value)
    assert test_dsn == expected
