from fastapi import APIRouter

from src.healthz.schema import HealthyRespond


healthz_router = APIRouter()


@healthz_router.get("/healthz", response_model=HealthyRespond)
async def healthz() -> HealthyRespond:
    return HealthyRespond()
