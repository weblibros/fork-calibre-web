import asyncio

import pytest
import pytest_asyncio
from httpx import AsyncClient, Headers
from pydantic import EmailStr

from src.authorisation.token import JWTToken, JWTUserAccesToken
from src.config.database import get_async_session, init_test_db
from src.main import app
from src.users.queries.create import create_user


def bearer_token_header(token: JWTToken) -> Headers:
    return Headers({"Authorization": f"Bearer {token}"})


@pytest.fixture(scope="session")
async def user(session):
    user = await create_user(
        session,
        email=EmailStr("user@fake.com"),
        password="supersecret",
    )
    yield user
    await session.delete(user)
    await session.commit()


@pytest.fixture(scope="session")
async def access_token_user(user):
    yield JWTUserAccesToken.encode(user)


@pytest.fixture(scope="session")
async def auth_header_user(access_token_user):
    yield bearer_token_header(access_token_user)


@pytest.fixture(scope="session")
def event_loop():
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
async def session():
    async_session = get_async_session()
    async for session in async_session:
        yield session


@pytest.fixture(scope="session", autouse=True)
async def migrate_test_db():
    await init_test_db()


@pytest_asyncio.fixture(scope="session")
async def client():
    async with AsyncClient(app=app, base_url="http://0.0.0.0:8000") as client:
        yield client
