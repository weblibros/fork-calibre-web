from typing import List

import sentry_sdk
import uvicorn
from fastapi import APIRouter, FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_pagination import add_pagination
from sentry_sdk.integrations.fastapi import FastApiIntegration
from sentry_sdk.integrations.starlette import StarletteIntegration

from src.authentication.router import auth_router
from src.books.router import books_router
from src.config import log_config, settings
from src.healthz.router import healthz_router
from src.users.router import user_router

if settings.SENTRY_DNS:
    sentry_sdk.init(
        dsn=settings.SENTRY_DNS,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production,
        traces_sample_rate=1.0,
        environment=settings.ENVIRONMENT,
        integrations=[
            StarletteIntegration(transaction_style="endpoint"),
            FastApiIntegration(transaction_style="endpoint"),
        ],
    )

app = FastAPI(
    title=settings.PROJECT_NAME,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.CORS_ORIGIN_LIST,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

router_list: List[APIRouter] = [
    healthz_router,
    books_router,
    auth_router,
    user_router,
]

for router in router_list:
    app.include_router(router)


add_pagination(app)


@app.on_event("startup")
async def on_startup():
    pass


if __name__ == "__main__":
    is_local_development_env = settings.ENVIRONMENT == "local"
    uvicorn.run(
        "src.main:app",
        host=settings.UVICORN_HOST,
        port=settings.UVICORN_PORT,
        log_level=settings.LOG_LEVEL.lower(),
        log_config=log_config,
        use_colors=True,
        reload=is_local_development_env,
        proxy_headers=not is_local_development_env,
    )
