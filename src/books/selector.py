from uuid import UUID

from fastapi_pagination import Params
from fastapi_pagination.bases import AbstractPage
from fastapi_pagination.ext.async_sqlalchemy import paginate
from sqlalchemy.ext.asyncio import AsyncSession
from sqlmodel import select

from src.books.filters import BookFilter
from src.books.models import Book


async def select_books(
    session: AsyncSession, params: Params, book_filter=BookFilter()
) -> AbstractPage[Book]:
    base_query = select(Book)
    query = book_filter.filter(base_query)
    books = await paginate(session, query=query, params=params)
    return books


async def select_book_detail(session: AsyncSession, uuid: UUID) -> Book | None:
    query = select(Book).filter_by(uuid=uuid)
    result = await session.execute(query)
    book = result.scalar_one_or_none()
    return book
