from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException
from fastapi_pagination import Page, Params
from fastapi_pagination.bases import AbstractPage
from sqlalchemy.ext.asyncio import AsyncSession

from src.books.filters import BookFilter
from src.books.models import Book
from src.books.selector import select_book_detail, select_books
from src.config.database import get_async_session

books_router = APIRouter()


@books_router.get("/books", response_model=Page[Book])
async def books(
    session: AsyncSession = Depends(get_async_session),
    params: Params = Depends(),
    book_filter: BookFilter = Depends(),
) -> AbstractPage[Book]:
    return await select_books(session, params, book_filter)


@books_router.get("/books/{book_uuid}", response_model=Book)
async def get_book(
    book_uuid: UUID,
    session: AsyncSession = Depends(get_async_session),
) -> Book:
    book = await select_book_detail(session, book_uuid)
    if book is None:
        raise HTTPException(status_code=404, detail="Book not found")
    return book
