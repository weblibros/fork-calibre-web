from typing import Optional

from fastapi_filter.contrib.sqlalchemy import Filter

from src.books.models import Book


class BookFilter(Filter):
    name: Optional[str]

    class Constants(Filter.Constants):
        model = Book
