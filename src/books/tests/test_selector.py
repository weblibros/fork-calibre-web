from uuid import uuid4

import pytest
from fastapi_pagination import Page, Params

from src.books.filters import BookFilter
from src.books.models import Book
from src.books.selector import select_book_detail, select_books


@pytest.fixture(scope="class")
async def book_list(session):

    book_list = []
    for index in range(10):
        book = Book(name=f"book : {index}")
        book_list.append(book)
        session.add(book)
    await session.commit()
    yield book_list
    for book in book_list:
        await session.delete(book)
    await session.commit()


@pytest.mark.usefixtures("book_list")
class TestBooksSelector:
    async def test_select_all_books(self, session, book_list):

        books = await select_books(session, Params())
        assert isinstance(books, Page)
        assert len(books.items) == len(book_list)
        assert books.total == len(book_list)
        assert isinstance(books.items[0], Book)

    async def test_filter_books(self, session):

        book_name = "book : 3"
        books = await select_books(session, Params(), BookFilter(name=book_name))
        assert isinstance(books, Page)
        assert len(books.items) == 1
        assert books.total == 1
        assert isinstance(books.items[0], Book)
        assert books.items[0].name == book_name


@pytest.fixture(scope="class")
async def book_detail_selector(session):

    book = Book(name="book very detailed in selectors")
    session.add(book)
    await session.commit()
    yield book
    await session.delete(book)
    await session.commit()


@pytest.mark.usefixtures("book_detail_selector")
class TestBooksDetailSelector:
    async def test_get_book(self, session, book_detail_selector):
        book = await select_book_detail(session, book_detail_selector.uuid)
        assert isinstance(book, Book)
        assert book.uuid == book_detail_selector.uuid

    async def test_get_no_book(self, session):
        book = await select_book_detail(session, uuid4())
        assert book is None
