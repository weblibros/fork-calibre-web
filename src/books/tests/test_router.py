from uuid import uuid4

import pytest

from src.books.models import Book


@pytest.fixture
async def book_data_endpoint(session):

    book_1 = Book(name="test book1 data")
    book_2 = Book(name="test book2 extra")
    session.add(book_1)
    session.add(book_2)
    await session.commit()
    yield [book_1, book_2]

    await session.delete(book_1)
    await session.delete(book_2)
    await session.commit()


class TestListEndpoint:
    @pytest.mark.usefixtures("book_data_endpoint")
    async def test_books_endpoint(self, client):
        response = await client.get("/books")
        assert response.status_code == 200
        page_books = response.json()
        books = page_books["items"]
        assert isinstance(books, list)
        assert len(books) == 2
        assert isinstance(books[0], dict)


@pytest.fixture(scope="class")
async def book_detail(session):

    book = Book(name="book very detailed in router")
    session.add(book)
    await session.commit()
    yield book
    await session.delete(book)
    await session.commit()


@pytest.mark.usefixtures("book_detail")
class TestBooksDetailSelector:
    async def test_detail_book_endpoint(self, client, book_detail):
        response = await client.get(f"/books/{book_detail.uuid}")
        assert response.status_code == 200
        book = response.json()
        assert isinstance(book, dict)
        assert book["uuid"] == str(book_detail.uuid)

    async def test_get_no_book(self, client):
        response = await client.get(f"/books/{uuid4()}")
        assert response.status_code == 404
