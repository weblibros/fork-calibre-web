from uuid import UUID, uuid4

from sqlmodel import Field, SQLModel


class Book(SQLModel, table=True):
    uuid: UUID = Field(
        default_factory=uuid4,
        primary_key=True,
        nullable=False,
    )
    name: str
