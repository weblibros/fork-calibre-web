from src.main import app
import json

if __name__ == "__main__":

    with open("openapi.json", "w") as f:
        json.dump(
            app.openapi(),
            f,
        )
