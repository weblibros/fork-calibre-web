from src.authentication.password_hasher import hash_password, verify_password


def test_hash_password():
    super_password = "12345"
    hashed_password = hash_password(super_password)

    assert isinstance(hashed_password, str)
    assert super_password != hashed_password
    assert "pbkdf2" in hashed_password
    assert "pbkdf2-sha512" in hashed_password


def test_no_unique_hash_password():
    super_password = "12345"
    hashed_password_1 = hash_password(super_password)
    hashed_password_2 = hash_password(super_password)
    assert hashed_password_1 != hashed_password_2


def test_verify_password():
    super_password = "12345"
    hashed_password = hash_password(super_password)
    correct_password = verify_password(super_password, hashed_password)
    assert correct_password is True
