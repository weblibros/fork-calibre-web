import pytest
from pydantic import EmailStr

from src.authentication import urls
from src.authentication.password_hasher import hash_password
from src.authorisation.token import JWTToken
from src.users.models import User


@pytest.fixture(scope="session")
async def user_login(session):

    user = User(
        email=EmailStr("user_login@fake.com"),
        password=hash_password("supersecret"),
    )
    session.add(user)
    await session.commit()
    yield user
    await session.delete(user)
    await session.commit()


class TestUserAuthEndpoint:
    @pytest.mark.usefixtures("user_login")
    async def test_password_login_endpoint(self, client):
        response = await client.post(
            urls.PASSWORD_LOGIN_USER_URL,
            data={
                "username": "user_login@fake.com",
                "password": "supersecret",
            },
        )
        assert response.status_code == 200
        jwt_token_pair = response.json()
        assert "access_token" in jwt_token_pair.keys()
        assert "refresh_token" in jwt_token_pair.keys()
        assert "token_type" in jwt_token_pair.keys()
        assert JWTToken(jwt_token_pair["access_token"]).is_valid()
        assert JWTToken(jwt_token_pair["refresh_token"]).is_valid()
        assert jwt_token_pair["token_type"] == "bearer"

    async def test_wrong_password_login_endpoint(self, client):
        response = await client.post(
            urls.PASSWORD_LOGIN_USER_URL,
            data={
                "username": "user_login@fake.com",
                "password": "not_correct",
            },
        )
        assert response.status_code == 403
        error_response = response.json()
        assert error_response["detail"] == "incorrect login"

    async def test_no_user_login_endpoint(self, client):
        response = await client.post(
            urls.PASSWORD_LOGIN_USER_URL,
            data={
                "username": "user_does_not_exists_login@fake.com",
                "password": "supersecret",
            },
        )
        assert response.status_code == 403
        error_response = response.json()
        assert error_response["detail"] == "incorrect login"
