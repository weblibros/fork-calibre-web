from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.ext.asyncio import AsyncSession

from src.authentication import password_hasher, urls
from src.authorisation.schema import JwtKeyPair
from src.authorisation.token_factory import user_jwt_tokens_factory
from src.config.database import get_async_session
from src.users.queries.selector import find_user_by_email

auth_router = APIRouter()


@auth_router.post(urls.PASSWORD_LOGIN_USER_URL, response_model=JwtKeyPair)
async def auth_user_by_password(
    session: AsyncSession = Depends(get_async_session),
    form_data: OAuth2PasswordRequestForm = Depends(),
):
    exception = HTTPException(
        status_code=403,
        detail="incorrect login",
    )

    user = await find_user_by_email(session, form_data.username)

    if user is None:
        raise exception

    valid_password = password_hasher.verify_password(
        raw_password=form_data.password,
        hashed_password=user.password,
    )
    if not valid_password:
        raise exception
    return user_jwt_tokens_factory(user)
