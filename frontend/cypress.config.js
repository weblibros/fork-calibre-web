module.exports = {
	e2e: {
		experimentalRunAllSpecs: true,
		setupNodeEvents(on, config) {
			// implement node event listeners here
		},
	},

	component: {
		devServer: {
			framework: "vue",
			bundler: "vite",
		},
	},
};
