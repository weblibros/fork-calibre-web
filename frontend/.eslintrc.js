module.exports = {
	root: true,
	parser: "vue-eslint-parser",
	parserOptions: {
		parser: "@typescript-eslint/parser",
	},
	extends: [
		"plugin:vue/strongly-recommended",
		"eslint:recommended",
		"@vue/typescript/recommended",
		"prettier",
		"plugin:cypress/recommended",
		"./.eslintrc-auto-import.json",
	],
	plugins: ["@typescript-eslint", "prettier", "cypress"],
	rules: {
		"prettier/prettier": "error",
		"@typescript-eslint/ban-ts-comment": "off",
		"vue/multi-word-component-names": "off",
		"vue/no-multiple-template-root": "off",
		// not needed for vue 3
		/* "vue/no-multiple-template-root": "off", */
	},
	env: {
		"cypress/globals": true,
		es6: true,
		browser: true,
		node: true,
		jest: true,
	},
};
