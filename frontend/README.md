## Usage

### Development

```bash
yarn dev
```

### Preview in Https

Just run and visit https://localhost

```bash
yarn build && yarn run https-preview
```

### Build

To build the App, run

```bash
yarn build
```

And you will see the generated file in `dist` that ready to be served.
