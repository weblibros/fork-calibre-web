import { defineStore, acceptHMRUpdate } from "pinia";
import JwtToken from "@/composables/jwt";
import Cookies from "js-cookie";
import { axiosApiV1NoTokenForm } from "@/composables/axios";
import { LoginForm } from "./types";

export const useJwtStore = defineStore("jwtStore", {
	state: () => {
		return {
			accesstoken: {
				token: Cookies.get("webLibrosJwtAccessToken") || "",
				payload: Cookies.get("webLibrosJwtAccessToken")
					? new JwtToken(
							Cookies.get("webLibrosJwtAccessToken") || "unvalidToken"
					  ).decode()
					: undefined,
			},
			refreshtoken: {
				token: Cookies.get("webLibrosJwtRefreshToken") || "",
				payload: Cookies.get("webLibrosJwtRefreshToken")
					? new JwtToken(
							Cookies.get("webLibrosJwtRefreshToken") || "unvalidToken"
					  ).decode()
					: undefined,
			},
		};
	},
	actions: {
		getToken(data: LoginForm) {
			return new Promise((resolve, reject) => {
				axiosApiV1NoTokenForm.post("/auth/user/", data).then(
					response => {
						const accesstoken = response.data.access;
						const refreshtoken = response.data.refresh;
						this.setAccessToken(accesstoken);
						this.setRefreshToken(refreshtoken);
						Cookies.set("webLibrosJwtAccessToken", accesstoken, {
							expires: 30,
						});
						Cookies.set("webLibrosJwtRefreshToken", refreshtoken, {
							expires: 30,
						});

						resolve(response);
					},
					error => {
						reject(error);
					}
				);
			});
		},
		setAccessToken(token: string) {
			const JwtAccessToken = new JwtToken(token);
			this.accesstoken = {
				token: token,
				payload: JwtAccessToken.decode(),
			};
		},
		setRefreshToken(token: string) {
			const JwtRefreshToken = new JwtToken(token);
			this.refreshtoken = { token: "", payload: undefined };
			this.refreshtoken = {
				token: token,
				payload: JwtRefreshToken.decode(),
			};
		},
		deleteTokens() {
			Cookies.remove("webLibrosJwtAccessToken");
			Cookies.remove("webLibrosJwtRefreshToken");

			this.refreshtoken = { token: "", payload: undefined };
			this.accesstoken = { token: "", payload: undefined };
		},
	},
});

if (import.meta.hot) {
	import.meta.hot.accept(acceptHMRUpdate(useJwtStore, import.meta.hot));
}
