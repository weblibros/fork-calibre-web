export type LoginForm = {
	password: string;
	username: string;
	scope?: string;
	client_id?: string;
	client_secret?: string;
};
