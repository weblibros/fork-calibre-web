import axios from "axios";
import JwtToken from "../jwt";
import Cookies from "js-cookie";

const defaultHeaders = {
	"Content-Type": "application/json",
	"Access-Control-Allow-Origin": "*",
};

const url = "http://0.0.0.0:8000/api/v1";

const axiosApiV1 = axios.create({
	baseURL: url,
	headers: defaultHeaders,
});
const axiosApiV1NoTokenForm = axios.create({
	baseURL: url,
	headers: {
		"content-type": "application/x-www-form-urlencoded",
	},
});

/* The interceptor here ensures that we check for the token in local storage every time an axios request is made
 */

const valid_access_token = () => {
	return new Promise((resolve, reject) => {
		const raw_accesstoken = Cookies.get("webLibrosJwtAccessToken");
		if (!raw_accesstoken) {
			//no token stored in cookies
			console.log("No access tokens found in cookies");
			return reject;
		}

		const accesstoken = new JwtToken(raw_accesstoken);
		if (!accesstoken.is_expired()) {
			// access token is valid
			return resolve(raw_accesstoken);
		}

		const refresh_token = new JwtToken(
			Cookies.get("webLibrosJwtRefreshToken") || "notValid"
		);
		if (!refresh_token.is_expired()) {
			// TODO Refresh token
		}
		// both tokes expired -> force reload
		remove_cookies_and_reload();
	});
};

const remove_cookies_and_reload = () => {
	Cookies.remove("webLibrosJwtAccessToken");
	Cookies.remove("webLibrosJwtRefreshToken");
	location.reload();
};

axiosApiV1.interceptors.request.use(
	async config => {
		const accesstoken = await valid_access_token();
		config.headers.Authorization = `Bearer ${accesstoken}`;
		return config;
	},
	error => Promise.reject(error)
);

export default axiosApiV1;
export { axiosApiV1NoTokenForm };
