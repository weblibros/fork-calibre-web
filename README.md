# Web Libros

## About

This project is a Fork of [Calibre Web](https://github.com/janeczku/calibre-web)  
Originally, it was planned to refactor the code base.  
However, the decision was made to start a new project from scratch `./src/**` parallel to the original fork `./cps/**` , as well a new frontend as a SPA `./frontend/**`.  
Once the project is mature enough, the original fork project will be removed and the project renamed.

roadmap:

- [x] containerasation of the project
  - [x] create a docker and docker compose file for developement [calibre-web]
  - [x] create a docker and docker compose file for developement [Backend]
  - [x] create a docker and docker compose file for developement [Frontend]
- [ ] setup python backend
  - [x] write test with pytest
  - [x] setup CI pipelines
  - [x] Use SQLModel
  - [x] enable migrations Alembic
  - [ ] write first endpoints for authenication (WIP)
    - [x] authentication endpoint
    - [ ] token refresh endpoint
    - [ ] reset password endpoint
    - [ ] invite users endpoint
    - [ ] onboarding endpoint
- [ ] setup vue frontend
  - [x] Use Vue3 for the frontend
  - [ ] create Login logic (WIP)
  - [ ] frontend unit testing
  - [ ] frontend testing with cypress
  - [ ] setup CI pipelines
  - [ ] setup storybook
- [ ] CD
  - [ ] auto deploy Backend
  - [ ] auto deploy Frontend
- [ ] features
  - [ ] Libary: view all your ebooks
  - [ ] upload new ebook
  - [ ] import calibre books
  - [ ] sync functionality with kobo ereaders
  - [ ] convert formats ebooks
  - [ ] share books with other users
  - [ ] create book shels
  - [ ] ...

## database migrations

for the databse migration Alembic is used.

new migrations are created with the following command

```
alembic revision --autogenerate -m "very smart comment"
```

migrate to latest version

```
alembic upgrade head
```
