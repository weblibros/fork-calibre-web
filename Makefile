
CONTAINER_NAME_BACKEND=web_libros
CONTAINER_NAME_FRONTEND=spa_web_libros
SPA_NODE_MODULES_VOLUME=web_libros_node_modules
COMPOSE_LOCAL=docker-compose.yaml -f docker-compose-spa.yaml -f docker-compose-ci-postgres.yaml




## DOCKER COMPOSE
.PHONY: docker
docker:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker compose -f $(COMPOSE_LOCAL) up ${ARGS}
.PHONY: docker-d
docker-d:
	ARGS=-d make docker
.PHONY: docker-build
docker-build:
	COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker compose -f $(COMPOSE_LOCAL) build ${ARGS}
.PHONY: docker-build-clean
docker-build-clean:
	ARGS=--no-cache make docker-build
.PHONY: docker-stop
docker-stop:
	docker compose -f $(COMPOSE_LOCAL) stop ${ARGS}
##---------------

## DOCKER
.PHONY: exec-backend
exec-backend:
	docker exec -it $(CONTAINER_NAME_BACKEND) bash ${ARGS}
.PHONY: exec-spa
exec-spa:
	docker exec -it $(CONTAINER_NAME_FRONTEND) bash ${ARGS}

.PHONY: exec-calibre
exec-calibre:
	docker exec -it calibre_web bash ${ARGS}
##---------------

## test
.PHONY: test
test:
	make test-backend
	make test-spa

.PHONY: test-backend
test-backend:
	ARGS=commands/test.sh make exec-backend
.PHONY: test-spa
test-spa:
	ARGS="-c 'yarn coverage'" make exec-spa

## CLEANUP
clean-spa:
	docker container stop $(CONTAINER_NAME_FRONTEND)
	docker container rm $(CONTAINER_NAME_FRONTEND)
	docker volume rm $(SPA_NODE_MODULES_VOLUME)
##---------------

